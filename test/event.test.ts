import * as assert from "assert";
import { describe, it } from "mocha";
import "reflect-metadata";
import * as mockito from "ts-mockito";
import { ConnectionPg } from "../src/databases/postgres/connection";
import { LoggerPg } from "../src/databases/postgres/logger/logger";
import { Migration } from "../src/databases/postgres/migration";
import { Tables } from "../src/databases/postgres/tables";
import { DbEventRepository } from "../src/repository/event/db-event"

// TODO add test config to add values to test db
// TODO add e2e test with superagent request
describe("event add check", () => {

    it("Should add to event into repository", async () => {
        const data = [
            {
                "id": "12324234",
                "msg": "test request",
                "query": {
                    "limit": 10,
                    "order": [
                        [
                            "QueryLog.duration",
                            "desc"
                        ]
                    ],
                    "measures": [
                        "QueryLog.duration"
                    ],
                    "timeDimensions": [
                        {
                            "dimension": "QueryLog.createdAt"
                        }
                    ]
                },
                "sentAt": "2021-10-14T16:18:09.841Z",
                "requestId": "test_request_id",
                "timestamp": "2021-10-14T16:18:09.126Z",
                "securityContext": {
                    "exp": 1634282247,
                    "iat": 1634195847
                }
            }
        ];

        const mockLoggerPg = mockito.mock<LoggerPg>();
        const mockMigration = mockito.mock<Migration>();

        const connectionPg = await (new ConnectionPg(mockLoggerPg, mockMigration));
        const eventRepository = new DbEventRepository(connectionPg);
        const db = await connectionPg.connect();
        await eventRepository.add(data);

        const getDbEvent = await db(Tables.Event)
            .select("data")
            .where("request_id", "test_request_id")
            .first();

        assert.deepStrictEqual(getDbEvent.data, data[0]);
    });
});
