import { Container } from "inversify";
import * as mongoDB from "mongodb";
import { EventController } from "../controllers/event";
import { ConnectionMongo } from "../databases/mongodb/connection";
import { ConnectionPg } from "../databases/postgres/connection";
import { Logger } from "../databases/postgres/logger/abstract";
import { LoggerPg } from "../databases/postgres/logger/logger";
import { Migration } from "../databases/postgres/migration";
import { EventRepository } from "../repository/event/abstract";
import { DbEventRepository } from "../repository/event/db-event";
import { Types } from "./types";

export const container = new Container();

container.bind<Promise<mongoDB.Db>>(Types.DbInstanceMongo).toConstantValue((new ConnectionMongo()).connect());
container.bind<ConnectionPg>(Types.DbInstancePg).to(ConnectionPg);
container.bind<Logger>(Types.LoggerPg).to(LoggerPg);
container.bind<Migration>(Types.MigrationPg).to(Migration);
container.bind<EventController>(Types.EventController).to(EventController);
container.bind<EventRepository>(Types.DbEventRepository).to(DbEventRepository);
