export const Types = {
    DbInstanceMongo: Symbol.for("DbInstanceMongo"),
    DbInstancePg: Symbol.for("DbInstancePg"),
    LoggerPg: Symbol.for("LoggerPg"),
    MigrationPg: Symbol.for("MigrationPg"),
    DbEventRepository: Symbol.for("DbEventRepository"),
    EventController: Symbol.for("EventController"),
}
