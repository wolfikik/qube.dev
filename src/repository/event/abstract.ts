export interface EventRepository {
    add(data: Array<Event>): Promise<void>;
}
