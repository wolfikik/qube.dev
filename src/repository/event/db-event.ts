import { inject, injectable } from "inversify";
import { Knex } from 'knex';
import { ConnectionPg } from "../../databases/postgres/connection";
import { Tables } from "../../databases/postgres/tables";
import { Types } from "../../inversify/types";
import { EventRepository } from "./abstract";

@injectable()
export class DbEventRepository implements EventRepository {

    constructor(@inject(Types.DbInstancePg) private readonly instance: ConnectionPg) {
    }

    public async add(data: Array<any>): Promise<void> {
        const db = await this.instance.connect();

        await db(Tables.Event).insert(data.map((field) => {
            return { data: field, request_id: field.requestId }
        }));
    };
}


/*import { inject, injectable } from "inversify";
import { EventRepository } from "./abstract";
import { Types } from "../../inversify/types";
import * as mongoDB from "mongodb";
import { Collections } from "../../databases/mongodb/collections";

@injectable()
export class DbEventRepository implements EventRepository {

    private collectionName: string = Collections.Event;

    constructor(@inject(Types.DbInstanceMongo) private readonly instance: Promise<mongoDB.Db>) {
    }

    public async add(data: Array<any>): Promise<void> {
        const db = await this.instance;
        const collection = await db.collection(this.collectionName);
        await collection.insertMany(data);
    };
}*/
