CREATE TABLE event(event_id serial PRIMARY KEY, data jsonb, request_id VARCHAR(128), created_at TIMESTAMP NOT NULL DEFAULT now());
CREATE TABLE query_log(query_id serial PRIMARY KEY, created_at TIMESTAMP NOT NULL DEFAULT now(), duration INT, query TEXT, bindings jsonb);
