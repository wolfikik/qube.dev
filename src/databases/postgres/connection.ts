import { inject, injectable } from "inversify";
import { knex, Knex } from "knex";
import { Types } from "../../inversify/types";
import { Config } from "../../lib/config";
import { ConnectionAbstract } from "../abstract";
import { Logger } from "./logger/abstract";
import { Migration } from "./migration";

@injectable()
export class ConnectionPg implements ConnectionAbstract {

    private instance: Knex;
    private database: string = Config.get("DB_PG_DATABASE");
    private host: string = Config.get("DB_PG_HOST");
    private user: string = Config.get("DB_PG_USER");
    private port: string = Config.get("DB_PG_PORT");
    private password: string = Config.get("DB_PG_PASSWORD");

    private getConnectionString() {
        return `postgres://${this.user}:${this.password}@${this.host}:${this.port}/${this.database}`;
    }

    constructor(
        @inject(Types.LoggerPg) private logger: Logger,
        @inject(Types.MigrationPg) private migration: Migration,
    ) {
    }

    public async connect(): Promise<Knex> {

        if (this.instance) {
            return this.instance
        }

        await this.migration.migrate();

        const config: Knex.Config = {
            client: "pg",
            connection: this.getConnectionString(),
        };

        const k = knex(config);
        await this.logger.log(k);

        this.instance = k;

        return k;
    }


}

