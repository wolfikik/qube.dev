import { injectable } from "inversify";
import { migrate, MigrateDBConfig } from "postgres-migrations";
import { Config } from "../../lib/config";

@injectable()
export class Migration {

    private database: string = Config.get("DB_PG_DATABASE");
    private host: string = Config.get("DB_PG_HOST");
    private user: string = Config.get("DB_PG_USER");
    private port: string = Config.get("DB_PG_PORT");
    private password: string = Config.get("DB_PG_PASSWORD");

    async migrate() {
        const dbConfig: MigrateDBConfig = {
            database: this.database,
            user: this.user,
            password: this.password,
            host: this.host,
            port: +this.port,
            ensureDatabaseExists: true,
            defaultDatabase: "postgres",
        }

        await migrate(dbConfig, Config.migrationPath);
    }
}
