import { injectable } from "inversify";
import { Knex } from "knex";
import now from "performance-now";
import { Tables } from "./tables";

@injectable()
export class LoggerPg {

    // logging queries to query_log table
    public async log(instance: Knex): Promise<void> {
        const times: { [k: string]: any } = {};

        instance.on('query', (query) => {
            // not good check but let it be
            if (!query.sql.toString().includes(Tables.QueryLog)) {
                const uid = query.__knexQueryUid;
                times[uid] = {
                    startTime: now(),
                    query: query.sql,
                    bindings: query.bindings,
                };
            }
        })
            .on('query-response', async (response, query) => {
                const uid = query.__knexQueryUid;

                if (times[uid]) {
                    const duration = Math.round(now() - times[uid].startTime);
                    await instance(Tables.QueryLog).insert({
                        duration,
                        query: times[uid].query,
                        bindings: JSON.stringify(times[uid].bindings)
                    });
                }

                delete times[uid];
            });

    }
}

