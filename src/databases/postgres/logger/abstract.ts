import { Knex } from "knex";

export interface Logger{
    log(instance: Knex): void
}
