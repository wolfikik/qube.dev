import { injectable } from "inversify";
import { Config } from "../../lib/config";
import { ConnectionAbstract } from "../abstract";


import * as mongoDB from "mongodb";

@injectable()
export class ConnectionMongo implements ConnectionAbstract {

    private static getConnectionString() {
        return `mongodb://${Config.get("DB_MONGO_USER")}:${Config.get("DB_MONGO_PASSWORD")}@${Config.get("DB_MONGO_HOST")}:${Config.get("DB_MONGO_PORT")}`;
    }

    public async connect(): Promise<mongoDB.Db> {
        const client: mongoDB.MongoClient = new mongoDB.MongoClient(ConnectionMongo.getConnectionString());
        await client.connect();

        return client.db(Config.get("DB_MONGO_DATABASE"));
    }
}

