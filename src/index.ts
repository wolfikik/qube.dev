import "reflect-metadata";

import { App } from "./application";
import { Config } from "./lib/config";

// TODO put errors into log
import { UncaughtErrorHandler } from "./lib/error-handlers";

process.on("unhandledRejection", UncaughtErrorHandler);
process.on("uncaughtException", UncaughtErrorHandler);

try {
    const application = App.application;
    application.listen(Config.get("WEB_PORT"));
} catch (error) {
    console.error(error);
}





