import { injectable } from "inversify";
import { Config } from "./config";

import * as mongoDB from "mongodb";

@injectable()
export class DbConnector {
    public async getMongo(): Promise<mongoDB.Db> {
        const client: mongoDB.MongoClient = new mongoDB.MongoClient(`mongodb://${Config.get("DB_USER")}:${Config.get("DB_PASSWORD")}@${Config.get("DB_HOST")}:${Config.get("DB_PORT")}`);
        await client.connect();

        return client.db("event");
    }
}

