import * as dotenv from "dotenv";
dotenv.config({ path: process.cwd() + '/.env' });

export class Config {
    public static migrationPath: string = process.cwd() + '/src/databases/postgres/migrations';

    public static get(name: string) {
        return process.env[name].toString();
    }
}
