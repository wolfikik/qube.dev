import { injectable } from "inversify";
import { model, Document, Schema } from 'mongoose';

@injectable()
export class EventModel {
    getModel() {
        const schema = new Schema<any>({ any: Schema.Types.Mixed });

        return model<Document>('Event', schema);
    }
}
