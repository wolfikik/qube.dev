import * as express from "express";
import { inject } from "inversify";
import {
    controller,
    httpPost,
    interfaces,
    request,
    response
} from "inversify-express-utils";
import { Types } from "../inversify/types";
import { EventRepository } from "../repository/event/abstract";

@controller("/event")
export class EventController implements interfaces.Controller {

    @httpPost("/")
    private async create(@request() req: express.Request, @response() res: express.Response) {
        try {
            await this.event.add(req.body);
            res.sendStatus(200);
        } catch (err) {
            console.log(err)
            res.status(400).json({ error: err.message });
        }
    }

    constructor(@inject(Types.DbEventRepository) private event: EventRepository) {
    }
}
