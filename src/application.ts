import * as bodyParser from "body-parser";
import { Application } from "express";
import { InversifyExpressServer } from "inversify-express-utils";
import { container } from "./inversify/container";

export class App {
    static get application(): Application {
        return App.init();
    }

    private static init() {
        const server = new InversifyExpressServer(container);
        server.setConfig((app) => {
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
        });

        return server.build();
    }
}
