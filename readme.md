**RUN**

cd ~/storage

docker-compose up -d

npm run start

**Description**

The application is implemented on node.js + typescript + postgres.
You can also quickly switch the collection of logs to mongodb (commented
here /src/repository/event/db-event.ts, I could have made a request to mongo
on another route, but did not, because of the not enough time).


Initially, mongodb is more suitable for the task, because the format of incoming data
does not depend on the application and it is better to store them not in SQL, but in NoSQL.
Only later I saw, there is no ready-made docker image for MongoDB Connector for BI.
Therefore, I decided to use postgres.


There are 2 schema files for a dashboard in the /dashboard/schema folder. 
In order not to throw a many of files, I apply schemas for cube.js 
and JSON queries for a dashboard that generate charts.


**Charts**
1. Requests per minute chart:

```
{
    "measures": [
        "Event.count"
    ],
    "timeDimensions": [
    {
        "dimension": "Event.createdAt",
        "granularity": "minute"
    }
    ],
    "order": {
        "Event.createdAt": "asc"
    },
    "filters": [],
    "limit": 10
}
```

2. Database queries per minute chart:
   
I assumed that only our application can connect to the database
```
{
    "measures": [
        "QueryLog.count"
    ],
    "timeDimensions": [
    {
        "dimension": "QueryLog.createdAt",
        "granularity": "minute"
    }
    ],
    "order": {
        "QueryLog.createdAt": "asc"
    }
}
```

3. Top 10 SQL queries by total execution time:

```
{
"measures": [
    "QueryLog.duration"
],
"timeDimensions": [
    {
        "dimension": "QueryLog.createdAt"
    }
],
"order": {
    "QueryLog.duration": "desc"
},
"dimensions": [
    "QueryLog.query",
    "QueryLog.bindings"
],
"limit": 10,
"filters": []
}
```
