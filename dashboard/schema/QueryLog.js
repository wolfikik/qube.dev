cube(`QueryLog`, {
  sql: `SELECT * FROM public.query_log`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [createdAt]
    },
    
    duration: {
      sql: `duration`,
      type: `max`
    }
  },
  
  dimensions: {
    bindings: {
      sql: `bindings`,
      type: `string`
    },
    
    query: {
      sql: `query`,
      type: `string`
    },
    
    createdAt: {
      sql: `created_at`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
