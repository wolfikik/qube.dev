cube(`Event`, {
  sql: `SELECT request_id, created_at FROM public.event as e GROUP BY request_id, created_at`,
  
  preAggregations: {

  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [requestId, createdAt]
    }
  },
  
  dimensions: {
    data: {
      sql: `data`,
      type: `string`
    },
    
    requestId: {
      sql: `request_id`,
      type: `string`
    },
    
    createdAt: {
      sql: `created_at`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
